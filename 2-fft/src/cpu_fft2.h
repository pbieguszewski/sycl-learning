#ifndef CPU_FFT2_H
#define CPU_FFT2_H

#include "Fft2Result.h"

Fft2Result compute_fft2_cpu(cv::Mat image);

#endif // CPU_FFT2_H
