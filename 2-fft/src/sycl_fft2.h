#ifndef SYCL_FFT2_H
#define SYCL_FFT2_H

#include "Fft2Result.h"

Fft2Result compute_fft2_sycl(cv::Mat image);

#endif // SYCL_FFT2_H
