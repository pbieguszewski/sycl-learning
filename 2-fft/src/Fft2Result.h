#ifndef FFT2_RESULT_H
#define FFT2_RESULT_H

#include <opencv2/core.hpp>

struct Fft2Result
{
  cv::Mat re;
  cv::Mat im;
};

#endif // FFT2_RESULT_H
