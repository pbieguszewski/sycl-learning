#include <chrono>
#include <optional>
#include <stdexcept>
#include <string_view>

#include <spdlog/spdlog.h>

#include <opencv2/opencv.hpp>

#include "cpu_fft2.h"
#include "sycl_fft2.h"

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    spdlog::error("Miss path to image!");
    return 1;
  }

  std::string_view imagePath = argv[1];
  spdlog::info("Loading image '{0}' file...", imagePath);

  cv::Mat image = cv::imread(imagePath.data(), cv::IMREAD_ANYCOLOR);
  if (image.empty())
  {
    spdlog::error("Cannot read the image: {0}", imagePath);
    return 1;
  }

  std::optional<Fft2Result> cpuResult;
  std::optional<Fft2Result> syclResult;

  spdlog::info("Computing fft2 via cpu...");

  try
  {
    const auto start = std::chrono::high_resolution_clock::now();
    cpuResult = compute_fft2_cpu(image);
    const auto stop = std::chrono::high_resolution_clock::now();

    const auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count();

    spdlog::info("Computed fft2 via CPU! Elapsed time {0} ns.", elapsed);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("Cannot compute fft2 via CPU! What: '{0}'", ex.what());
  }

  spdlog::info("Computing fft2 via SYCL...");

  try
  {
    const auto start = std::chrono::high_resolution_clock::now();
    syclResult = compute_fft2_sycl(image);
    const auto stop = std::chrono::high_resolution_clock::now();

    const auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count();

    spdlog::info("Computed fft2 via SYCL! Elapsed time {0} ns.", elapsed);
  }
  catch (const std::exception& ex)
  {
    spdlog::error("Cannot compute fft2 via SYCL. What: '{0}'", ex.what());
  }

  cv::imshow("Original image", image);

  if (cpuResult)
  {
    cv::imshow("Re part - CPU", cpuResult->re);
    cv::imshow("Im part - CPU", cpuResult->im);
  }

  if (syclResult)
  {
    cv::imshow("Re part - SYCL", syclResult->re);
    cv::imshow("Im part - SYCL", syclResult->im);
  }

  cv::waitKey();

  return 0;
}
