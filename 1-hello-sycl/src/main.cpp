#include <algorithm>
#include <functional>
#include <memory>

#include <CL/sycl.hpp>

#include <spdlog/spdlog.h>

constexpr double ALPHA = 0.65;
constexpr std::size_t ARRAY_LEN = 1024;

int main()
{
  try
  {
    sycl::default_selector deviceSelector;

    sycl::queue queue{ deviceSelector };

    std::unique_ptr<double, std::function<void(double*)>> array{
      sycl::malloc_shared<double>(ARRAY_LEN, queue), [=](double* arr) { sycl::free(arr, queue); }
    };

    if (!array)
    {
      spdlog::error("Cannot allocate array!");
      return -1;
    }

    std::generate(array.get(),
                  array.get() + ARRAY_LEN,
                  []()
                  {
                    static double i = 0;
                    i += 0.5;
                    return i;
                  });

    queue.parallel_for(ARRAY_LEN, [arr = array.get()](auto& i) { arr[i] = arr[i] * ALPHA; });

    queue.wait();

    auto msg = fmt::format("Result: [ {0}, ", *array.get());

    for (std::size_t i = 0; i < ARRAY_LEN - 1; i++)
    {
      msg += fmt::format("{0}, ", *(array.get() + i));
    }

    msg += fmt::format(" {0} ]", *(array.get() + ARRAY_LEN - 1));

    spdlog::info(msg);
  }
  catch (const sycl::exception& ex)
  {
    spdlog::error("sycl_exc: {0}", ex.what());
  }
  catch (const std::exception& ex)
  {
    spdlog::error("std_exc: {0}", ex.what());
  }
  catch (...)
  {
    spdlog::error("Unknown error");
  }

  return 0;
}
